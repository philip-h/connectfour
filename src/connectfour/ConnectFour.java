package connectfour;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author philipjnr.habib
 */
public class ConnectFour extends JFrame implements ActionListener
{
    private static final long serialVersionUID = 1L;
    private RoundedButton[][] chips = new RoundedButton[6][7];
    private JButton reset = new JButton("Reset");
    private boolean turn = true;
    private int cats = 0;

    public ConnectFour()
    {
        super("Philip's Awesome Connect Four!");
        setSize(new Dimension(400, 400));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        init();
        setVisible(true);
    }

    private void init()
    {
        JPanel mainPane = new JPanel(new BorderLayout());
        JPanel gridPane = new JPanel(new GridLayout(6, 7, 5, 5));
        for (int row = 0; row < chips.length; row++)
        {
            for (int col = 0; col < chips[row].length; col++)
            {
                chips[row][col] = new RoundedButton();
                chips[row][col].setBackground(Color.white);
                chips[row][col].addActionListener(this);

                gridPane.add(chips[row][col]);
            }
        }
        mainPane.add(gridPane, BorderLayout.CENTER);
        reset.addActionListener(this);
        mainPane.add(reset, BorderLayout.SOUTH);
        setContentPane(mainPane);

    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == reset)
        {
            reset();
        } else
        {
            RoundedButton buttonPressed = (RoundedButton) e.getSource();
            int column = getColumnOfPressedButton(buttonPressed);
            for (int row = 0; row < chips.length; row++)
            {
                if (row == 0 && chips[row][column].getBackground() != Color.white)
                {
                    //Do Nothing, not checking this causes unneeded errors
                } else
                {
                    if (chips[row][column].getBackground() != Color.white)
                    {
                        colourChip(chips[row - 1][column]);
                        cats++;
                        checkWin();
                        return;
                    } else if (row == chips.length - 1)
                    {
                        colourChip(chips[row][column]);
                        cats++;
                        checkWin();
                        return;
                    }
                }
            }
        }

    }

    private int getColumnOfPressedButton(RoundedButton buttonPressed)
    {
        for (int row = 0; row < chips.length; row++)
        {
            for (int col = 0; col < chips[row].length; col++)
            {
                if (buttonPressed.equals(chips[row][col]))
                {
                    return col;
                }
            }
        }
        return -1;
    }

    private void colourChip(JButton button)
    {
        if (button.getBackground() == Color.white)
        {
            if (turn)
            {
                button.setBackground(Color.red);
                turn = !turn;
            } else
            {
                button.setBackground(Color.blue);
                turn = !turn;
            }
        }

    }

    private void checkWin()
    {
        if (cats > 41)
        {
            JOptionPane.showMessageDialog(null, "Tie Game!");
            return;
        }

        for (int i = 0; i < chips.length; i++)
        {
            for (int j = 0; j < chips[i].length; j++)
            {
                //Check Vertical
                if (i < 3)
                {
                    Color col1 = chips[i][j].getBackground();
                    Color col2 = chips[i + 1][j].getBackground();
                    Color col3 = chips[i + 2][j].getBackground();
                    Color col4 = chips[i + 3][j].getBackground();
                    if (col1 == col2 && col2 == col3 && col3 == col4 && col1 != Color.white)
                    {
                        JOptionPane.showMessageDialog(null, "Congratz! " + (col1 == Color.blue ? "Blue" : "Red") + " wins!!");
                        chips[i][j].setBackground(Color.cyan);
                        chips[i + 1][j].setBackground(Color.cyan);
                        chips[i + 2][j].setBackground(Color.cyan);
                        chips[i + 3][j].setBackground(Color.cyan);
                        return;
                    }

                }

                //Check Horizontal
                if (j < 4)
                {
                    Color col1 = chips[i][j].getBackground();
                    Color col2 = chips[i][j + 1].getBackground();
                    Color col3 = chips[i][j + 2].getBackground();
                    Color col4 = chips[i][j + 3].getBackground();
                    if (col1 == col2 && col2 == col3 && col3 == col4 && col1 != Color.white)
                    {
                        JOptionPane.showMessageDialog(null, "Congratz! " + (col1 == Color.blue ? "Blue" : "Red") + " wins!!");
                        chips[i][j].setBackground(Color.cyan);
                        chips[i][j + 1].setBackground(Color.cyan);
                        chips[i][j + 2].setBackground(Color.cyan);
                        chips[i][j + 3].setBackground(Color.cyan);
                        return;
                    }
                }
            }
        }

        //Check Diagonal Down
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                Color col1 = chips[i][j].getBackground();
                Color col2 = chips[i + 1][j + 1].getBackground();
                Color col3 = chips[i + 2][j + 2].getBackground();
                Color col4 = chips[i + 3][j + 3].getBackground();
                if (col1 == col2 && col2 == col3 && col3 == col4 && col1 != Color.white)
                {
                    JOptionPane.showMessageDialog(null, "Congratz! " + (col1 == Color.blue ? "Blue" : "Red") + " wins!!");
                    chips[i][j].setBackground(Color.cyan);
                    chips[i + 1][j + 1].setBackground(Color.cyan);
                    chips[i + 2][j + 2].setBackground(Color.cyan);
                    chips[i + 3][j + 3].setBackground(Color.cyan);
                    return;
                }
            }
        }

        //Check Diagonal Up
        for (int i = 3; i < chips.length; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                Color col1 = chips[i][j].getBackground();
                Color col2 = chips[i - 1][j + 1].getBackground();
                Color col3 = chips[i - 2][j + 2].getBackground();
                Color col4 = chips[i - 3][j + 3].getBackground();
                if (col1 == col2 && col2 == col3 && col3 == col4 && col1 != Color.white)
                {
                    JOptionPane.showMessageDialog(null, "Congratz! " + (col1 == Color.blue ? "Blue" : "Red") + " wins!!");
                    chips[i][j].setBackground(Color.cyan);
                    chips[i - 1][j + 1].setBackground(Color.cyan);
                    chips[i - 2][j + 2].setBackground(Color.cyan);
                    chips[i - 3][j + 3].setBackground(Color.cyan);
                    return;
                }
            }
        }
    }

    private void reset()
    {
        for (RoundedButton[] chip : chips)
        {
            for (RoundedButton chip1 : chip)
            {
                chip1.setBackground(Color.white);
            }
        }
        cats = 0;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        new ConnectFour();
    }
}
